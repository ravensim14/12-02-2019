const capitalize = require('./capitalize');

describe('capitalize', function() {
    it('capitalize("triin") => "Triin"', function() {
        expect(capitalize('triin')).toBe('Triin');
    });
    it('capitalize("mati") => "Mati"', function() {
        expect(capitalize('mati')).toBe('Mati');
    });
    it('number are input for capitalize is error', function() {
        expect(function() {
            capitalize(12345);
        }).toThrow('input need to be string!');
    });
    it('capitalize("triin") is not "triin"', function() {
        expect(capitalize('triin')).not.toBe('triin');
    });
    it('capitalize("MatiM8") => "MatiM8"', function() {
        expect(capitalize('MatiM8')).toBe('MatiM8');
    });
});
